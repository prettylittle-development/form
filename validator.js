
import {dom} from '../dom/dom';
import {events} from '../events/events';
import EventManager from '../events/EventManager';
import {ElementData} from '../dom/ElementData';

let validator =
{
	init : function(root = document)
	{
		/*

		// TODO: enable/disable input buttons

		EventManager.add_action('form/field/invalid', function(input, message)
		{
			let form			= dom.getClosest(input, 'form');

			let buttons			= form.querySelectorAll('button');

			[].forEach.call(buttons, function(button, i)
			{
				//button.disabled	= true;
			});

		}, 10, 2);

		EventManager.add_action('form/field/valid', function(input, message)
		{
			let form			= dom.getClosest(input, 'form');
			//let valid			= EventManager.do_action('validateform', form, true);
		}, 10, 2);

		*/

		const KEY_ESC			= 27;
		const KEY_TAB			= 9;

		// watch for from submission attempts
		events.live('form[data-validate]', 'submit', function(ev)
		{
			// run the form validation action
			let valid			= EventManager.do_action('validateform', this, false);

			// if the form is not valid, cancel the submit event
			if(!valid)
			{
				events.cancel(ev);
			}
			else
			{
				alert("Form is valid - allow it to be submitted");
				events.cancel(ev);
			}
		});

		// watch for events on the inputs
		// TODO: implement for autocomplete, focus, blur etc
		events.live('form[data-validate] input, form[data-validate] textarea', 'input', function(ev)
		{
			switch(ev.keyCode)
			{
				case KEY_ESC:
				case KEY_TAB:
					break;

				default:
					// trigger the inline validation on the field
					EventManager.do_action('field/validate', this, false);
					break;
			}
		});

		// change event for select to trigger validation
		events.live('form[data-validate] select', 'change', function(ev)
		{
			EventManager.do_action('field/validate', this, false);
		});

		events.live('form[data-validate] input[type="checkbox"]', 'change', function(ev)
		{
			EventManager.do_action('field/validate', this, false);
		});

		// TODO: Validation for checkbox & radio

		// validate form action
		EventManager.add_action('validateform', function(form, supressed = false, update = true)
		{
			// by default the form is valid
			let valid			= true;

			// get all of the inputs in the form
			let inputs			= form.querySelectorAll('input, textarea, select');

			// loop through all of the inputs
			[].forEach.call(inputs, function(field, i)
			{
				// validate the field
				let message		= EventManager.do_action('field/validate', field, supressed, update);

				// if there is an error message, the field is no longer valid
				if(message !== '')
				{
					// if the form is currently valid, focus the field - it's the first field that
					// has an error
					if(valid === true && supressed === false)
					{
						// focus the field
						field.focus();

						// this block of code is used to make sure that the field is focused at the end
						// get the current value of the field
						let value	= field.value;

						// blank it out
						field.value	= "";

						// change the value back again after a short timeout
						setTimeout(function()
						{
							field.value	= value;
						}, 1);
					}

					// the form is no longer valid
					valid		= false;
				}
			});

			EventManager.do_action('form/validated', form, valid);

			// return the form valid state
			return valid;

		}, 10, 3);

		/**
		 * Helper function for format the displaying of the error message
		 * @param  {[type]} message      [description]
		 * @param  {[type]} field_params [description]
		 * @return {[type]}              [description]
		 */
		function get_formatted_message(message, field_params)
		{
			// TODO
			return message;
			return "<b>This field</b> " + message;
		}

 		/**
 		 * Get the details for a default valid field
 		 * @param  {[type]} field [description]
 		 * @return {[type]}       [description]
 		 */
		function get_valid_field(field)
		{
			// defaults for the field
			const defs			=
			{
				"required"			: false,
				"value"				: "",
				"data-validator"	: "",
				"label"				: "Field"
			};

			//
			let value			= field.value;
			let required		= field.required;

			let	params			= {};
			let atts			= field.attributes;
			let v				= null,
				n				= null;

			// by default set the field type to be the same as the node type - this wil be overridden for inputs
			params['type']		= field.nodeName.toLowerCase();

			// for through all the field attributes and add them into the attributes
			for(var i = 0; i < atts.length; i++)
			{
				n				= atts[i].name;
				v				= atts[i].value;

				// if the value is blank, or the name is the same as the value....
				if(v === "" || n === v)
				{
					switch(n)
					{
						case "required":
						case "disabled":
						case "readonly":
						case "multiple":
							v	= true;
							break;
					}
				}

				// convert the value to a boolean if necessary
				v				= v === "true" ? true : (v === "false" ? false : v);

				// add to the params list
				params[n]		= v;
			}

			// TODO: get values by name

			// make sure the value is set
			params['value']		= field.value;

			// make sure that the defaults and params are combined
			params				= Object.assign({}, defs, params);

			// TODO - sort this out, this should not be like this
			if(params['type'] === 'checkbox')
			{
				//
				let name		= params['name'];

				// get a list of related items to the checkbox (by name)
				let relations	= document.querySelectorAll('input[type="'+params['type']+'"][name="'+name+'"]');

				// if there are multiple relations
				if(relations.length > 1)
				{
					// blank out the value until we have validated everything... this should be an array now
					params['value']	= '';

					let arr		= [];

					// loop through all the relations
					[].forEach.call(relations, function(relation, i)
					{
						// if the input is checked, push it into the array list
						if(relation.checked === true)
						{
							arr.push(relation.value);
						}
					});

					// if an array of values has been populated, assign the new value
					if(arr.length)
					{
						params['value']		= arr;
					}
				}
				// no relations
				else
				{
					// if the is not checked, blank out the stored value
					if(field.checked === false)
					{
						params['value']		= '';
					}
				}
			}

			if(params['type'] === 'select')
			{
				if(params.multiple)
				{
					let values = [].slice.call(field.selectedOptions).map(function(a)
					{
						return a.value;
					});

					params['value']		= values;
				}
			}

			// if there is a labbeled by attribute, we can get the text for the label name for the
			// input, this will allow us to provide more customised error messages
			if(params['aria-labelledby'])
			{
				// get the label for the field
				let label		= document.querySelector('#' + params['aria-labelledby']);

				// if the label exists, set the label
				if(label)
				{
					params['label']		= label.innerHTML;
				}
			}

			// allow additional plugins to hook in and change/update the params
			params				= EventManager.apply_filters('field/get_valid', params, field);

			// return the valid field data
			return params;
		}

		EventManager.add_action('field/validate', function(field, supressed = false, update = true)
		{
			// get valid date for the field
			let params			= get_valid_field(field);

			// this is a feedback message associated with the field
			let result			= '';

			// check if the field is disabled or readonly, if it is, validation does not need to be applied
			if(field.disabled || field.readonly)
			{
				return result;
			}

			// check for the field being blank (just whitespace)
			if(typeof params['value'] === 'string' && params['value'].match(/^([\s]+)$/))
			{
				result			= 'cannot be blank';
			}
			// if the field is required and the value is blank, required message
			else if((params['required'] && params['value'] === '') || (params['required'] && params['value'] instanceof Array && params['value'].length === 0))
			{
				result			= 'is required';
			}
			// default
			else if(params['required'] || (!params['required'] && params['value'] !== ''))
			{
				// run the field validation hooks
				result			= EventManager.apply_filters('form/field', result, params['value'], params);

				// run field validation based on the input type
				result			= EventManager.apply_filters('form/field/type=' + params['type'], result, params['value'], params);

				// TODO: validate on pattern/regex

				// if there is a custom validator for the input, run it
				if(params['data-validator'])
				{
					// allow for the ability to stack validators up as a CSV
					let validators	= params['data-validator'].split(',');

					// loop through all the validators and apply the validation filter
					[].forEach.call(validators, function(validator, i)
					{
						result		= EventManager.apply_filters('form/field/validator=' + validator, result, params['value'], params);

						console.log('form/field/validator=' + validator)
					});
				}

				// run validation based on the input id
				result			= EventManager.apply_filters('form/field/id=' + params['id'], result, params['value'], params);
			}

			if(update)
			{
				let error_id		= params['id'] + '-error';

				if(params['data-errorid'])
				{
					error_id		= params['data-errorid'];
				}

				// get the panel for error messages associated with the input
				let error_panel		= document.getElementById(error_id);

				// if therre is an error message
				if(result !== '')
				{
					// set the field the be invalid
					field.setAttribute('aria-invalid', true);

					// if the validation is not supressed
					if(!supressed)
					{
						// if the error panel, display the error message
						if(error_panel)
						{
							error_panel.innerHTML	= get_formatted_message(result, params);
						}

						// trigger any additional invali field actions
						EventManager.do_action('form/field/invalid', field);
					}
				}
				else
				{
					// if there is an error panel, make sure there's no messagin displayed, as the field is now valid
					if(error_panel)
					{
						error_panel.innerHTML	= "";
					}

					// if the field is not required, and the value is blank...
					if(!params['required'] && params['value'] === '')
					{
						field.removeAttribute('aria-invalid');
					}
					// the field is valid
					else
					{
						field.setAttribute('aria-invalid', false);

						// if the validation is not supressed
						if(!supressed)
						{
							// trigger any additional valid field actions
							EventManager.do_action('form/field/valid', field);
						}
					}
				}
			}

			return result;
		}, 10, 3);

		// -----------------------------------------------------------------------------------------
		// Form field validation methods
		// -----------------------------------------------------------------------------------------

		function validation_range(message, value, field)
		{
			let min				= field['min'] ? Number(field['min']) : null,
				max				= field['max'] ? Number(field['max']) : null;

			value				= Number(value);

			if(min != null && max != null)
			{
				if(min === max && value !== min)
				{
					message		= `should be ${min}`;
				}
				else if(value < min || value > max)
				{
					message		= `should be between ${min} and ${max}`;
				}
			}
			else if(min != null)
			{
				if(value < min)
				{
					message		= `should be at least ${min}`;
				}
			}
			else if(max != null)
			{
				if(value > max)
				{
					message		= `should be no greater than ${max}`;
				}
			}

			return message;
		}

		function validation_length(message, value, field)
		{
			let min				= field['data-minlength'] ? Number(field['data-minlength']) : null,
				max				= field['data-maxlength'] ? Number(field['data-maxlength']) : null,
				len				= value.length;

			if(min != null && max != null)
			{
				if(min === max && len !== min)
				{
					message		= `should be ${min} characters`;
				}
				else if(len < min || len > max)
				{
					message		= `should be between ${min} and ${max} characters`;
				}
			}
			else if(min != null)
			{
				if(len < min)
				{
					message		= `should be at least ${min} characters`;
				}
			}
			else if(max != null)
			{
				if(len > max)
				{
					message		= `should be at most ${max} characters`;
				}
			}

			return message;
		}

		function validation_equal(message, value, field)
		{
			if(message === '' && field['data-equal'])
			{
				let match		= document.querySelector(field['data-equal']);

				if(match)
				{
					match		= get_valid_field(match);

					if((field['value'] !== match['value']))
					{
						message	= `must equal <em>${match['label']}</em>`;
					}
				}
			}

			return message;
		}

		function validation_notequal(message, value, field)
		{
			if(message === '' && field['data-notequal'])
			{
				let match		= document.querySelector(field['data-notequal']);

				if(match)
				{
					match		= get_valid_field(match);

					if((field['value'] === match['value']))
					{
						message	= `must be different to <em>${match['label']}</em>`;
					}
				}
			}

			return message;
		}

		function validation_checked(message, value, field)
		{
			let min				= field['data-minchecked'] ? Number(field['data-minchecked']) : null,
				max				= field['data-maxchecked'] ? Number(field['data-maxchecked']) : null;

			// TODO: check for an actual array
			if(typeof value === 'string')
			{
				value			= [value];
			}

			let len				= value.length;

			if(min != null && max != null)
			{
				if(min === max && len !== min)
				{
					message		= `should be ${min} checked`;
				}
				else if(len < min || len > max)
				{
					message		= `should be between ${min} and ${max} checked`;
				}
			}
			else if(min != null)
			{
				if(len < min)
				{
					message		= `should be at least ${min} checked`;
				}
			}
			else if(max != null)
			{
				if(len > max)
				{
					message		= `should be at most ${max} checked`;
				}
			}

			return message;
		}

		EventManager.add_filter('form/field', validation_length, 10, 3);

		EventManager.add_filter('form/field/type=checkbox', validation_checked, 10, 3);

		EventManager.add_filter('form/field/type=color', function(message, value, field)
		{
			return message;
		}, 10, 3);

		function get_date(value)
		{
			// YYYY-MM-DD
			let date_format		= /^(\d{4})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/;
			let date_match		= value.match(date_format);

			if(date_match)
			{
				return new Date(Number(date_match[1]), Number(date_match[2]) - 1, Number(date_match[3]));
			}

			// DD/MM/YYYY
			date_format			= /^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])\/(\d{4})$/;
			date_match			= value.match(date_format);

			if(date_match)
			{
				return new Date(Number(date_match[3]), Number(date_match[2]) - 1, Number(date_match[1]));
			}

			return false;
		}

		EventManager.add_filter('form/field/type=date', function(message, value, field)
		{
			let d					= get_date(value);

			if(!d)
			{
				message				= "invalid date";
			}
			else
			{
				try
				{
					let min			= field['min'] ? get_date(field['min']) : null,
						max			= field['max'] ? get_date(field['max']) : null;

					if(min != null && max != null)
					{
						if(min === max && d !== min)
						{
							message	= `should be ${min}`;
						}
						else if(d < min || d > max)
						{
							message	= `should be between ${min} and ${max}`;
						}
					}
					else if(min != null)
					{
						if(d < min)
						{
							message	= `should be at least ${min}`;

							if(field['data-minmsg'])
							{
								message		= field['data-minmsg'];
							}
						}
					}
					else if(max != null)
					{
						if(d > max)
						{
							message	= `should be no greater than ${max}`;

							if(field['data-maxmsg'])
							{
								message		= field['data-maxmsg'];
							}
						}
					}
				}
				catch(e)
				{
					console.warn(e);
				}
			}

			return message;
		}, 10, 3);

		EventManager.add_filter('form/field/type=datetime', function(message, value, field)
		{
			return message;
		}, 10, 3);

		EventManager.add_filter('form/field/type=datetime-local', function(message, value, field)
		{
			return message;
		}, 10, 3);

		EventManager.add_filter('form/field/type=email', function(message, value, field)
		{
			let pattern			= /^[a-z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/i;

			if(!pattern.exec(value))
			{
				message			= 'invalid email';
			}

			return message;
		}, 10, 3);

		EventManager.add_filter('form/field/type=file', function(message, value, field)
		{
			return message;
		}, 10, 3);

		EventManager.add_filter('form/field/type=hidden', function(message, value, field)
		{
			return message;
		}, 10, 3);

		EventManager.add_filter('form/field/type=month', function(message, value, field)
		{
			return message;
		}, 10, 3);

		EventManager.add_filter('form/field/type=number', validation_range, 10, 3);

		function validation_password(message, value, field)
		{
			if(!((value.length >= 8) && (/[A-Z]/.test(value)) && (/[a-z]/.test(value)) && (/[0-9]/.test(value))))
			{
				message				= 'invalid password';
			}

			// TODO: move
			if(field['data-helper'])
			{
				let checks		= document.querySelectorAll(field['data-helper'] + ' [data-match]');

				[].forEach.call(checks, function(check, i)
				{
					let pattern	= check.getAttribute('data-match');
					let rE		= new RegExp(pattern);

					if(value.match(rE))
					{
						dom.addClass(check, 'valid');
						dom.removeClass(check, 'invalid');
					}
					else
					{
						dom.addClass(check, 'invalid');
						dom.removeClass(check, 'valid');
					}
				});
			}

			return message;
		}

		EventManager.add_filter('form/field/type=password', validation_password, 10, 3);
		EventManager.add_filter('form/field/type=password', validation_equal, 10, 3);
		EventManager.add_filter('form/field/type=password', validation_notequal, 10, 3);

		EventManager.add_filter('form/field/validator=password', validation_password, 10, 3);
		EventManager.add_filter('form/field/validator=password', validation_equal, 10, 3);
		EventManager.add_filter('form/field/validator=password', validation_notequal, 10, 3);

		EventManager.add_filter('form/field/type=radio', function(message, value, field)
		{
			return message;
		}, 10, 3);

		EventManager.add_filter('form/field/type=range', validation_range, 10, 3);

		EventManager.add_filter('form/field/type=search', function(message, value, field)
		{
			return message;
		}, 10, 3);

		EventManager.add_filter('form/field/type=select', validation_checked, 10, 3);

		EventManager.add_filter('form/field/type=select', function(message, value, field)
		{
			return message;
		}, 10, 3);

		EventManager.add_filter('form/field/type=tel', function(message, value, field)
		{
			return message;
		}, 10, 3);

		EventManager.add_filter('form/field/type=text', function(message, value, field)
		{
			return message;
		}, 10, 3);

		EventManager.add_filter('form/field/type=textarea', function(message, value, field)
		{
			return message;
		}, 10, 3);

		EventManager.add_filter('form/field/type=url', function(message, value, field)
		{
			let re				= /^(http|https):\/\/(([a-zA-Z0-9$\-_.+!*'(),;:&=]|%[0-9a-fA-F]{2})+@)?(((25[0-5]|2[0-4][0-9]|[0-1][0-9][0-9]|[1-9][0-9]|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9][0-9]|[1-9][0-9]|[0-9])){3})|localhost|([a-zA-Z0-9\-\u00C0-\u017F]+\.)+([a-zA-Z]{2,}))(:[0-9]+)?(\/(([a-zA-Z0-9$\-_.+!*'(),;:@&=]|%[0-9a-fA-F]{2})*(\/([a-zA-Z0-9$\-_.+!*'(),;:@&=]|%[0-9a-fA-F]{2})*)*)?(\?([a-zA-Z0-9$\-_.+!*'(),;:@&=\/?]|%[0-9a-fA-F]{2})*)?(\#([a-zA-Z0-9$\-_.+!*'(),;:@&=\/?]|%[0-9a-fA-F]{2})*)?)?$/;

			if(!re.test(value))
			{
				message			= "invalid url";
			}

			return message;
		}, 10, 3);

		EventManager.add_filter('form/field/type=week', function(message, value, field)
		{
			let html5_format	= /([\d]+)-W([\d]+)/,
				std_format		= /([\d]+)-([\d]+)/;

			return message;
		}, 10, 3);

		// get a list of all the forms on the page
		let forms					= document.querySelectorAll('form[data-validate]');

		// loop through all the forms
		[].forEach.call(forms, function(form, i)
		{
			EventManager.do_action('validateform', form, true, false);
		});

	}
}

export default validator;
