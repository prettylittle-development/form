
import EventManager from '../../events/EventManager';

EventManager.add_filter('form/field/validator=creditcard', function(message, value, field)
{
	// strip out any whitespace
	value						= value.replace(/([\s]+)/g, "");

	// check that the value is 16 digits...
	if(value.length !== 16 || !/^\d+$/.test(value))
	{
		return "Please enter your 16 digit card number";
	}

	return message;
}, 10, 3);
