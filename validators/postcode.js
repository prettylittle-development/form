
import EventManager from '../../events/EventManager';

EventManager.add_filter('form/field/validator=postcode', function(message, value, field)
{
	let valid					= value.toUpperCase().match(/^[A-Z]{1,2}[0-9R][0-9A-Z]?([\s])?[0-9][ABD-HJLNP-UW-Z]{2}$/);

	if(!valid)
	{
		return "Please enter a valid UK postcode";
	}

	return message;
}, 10, 3);
