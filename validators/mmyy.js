
import EventManager from '../../events/EventManager';

EventManager.add_filter('form/field/validator=mm/yy', function(message, value, field)
{
	let parts					= value.split('/');

	if(parts.length)
	{
		let mm					= Number(parts[0]),
			yy					= Number(parts[1]);

		if((!parts[0] || !parts[1]) || (isNaN(mm) || isNaN(yy)))
		{
			return "Please enter a valid date";
		}

		if(mm < 1 || mm > 12)
		{
			return "Please enter a valid month";
		}

		let now				= new Date(),
			then			= new Date(2000 + yy, mm, now.getDate());

		if(then <= now)
		{
			return "Please enter a date in the future"
		}
	}

	return message;
}, 10, 3);
