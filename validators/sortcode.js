
import EventManager from '../../events/EventManager';

EventManager.add_filter('form/field/validator=sortcode', function(message, value, field)
{
	// strip out any whitespace
	value						= value.replace(/([\s]+)/g, "");

	// check that the value is 6 digits...
	if(value.length !== 6 || !/^\d+$/.test(value))
	{
		return "Please enter your 6 digit sort code";
	}

	return message;
}, 10, 3);
