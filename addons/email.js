
import {dom} from '../../dom/dom';
import {events} from '../../events/events';
import EventManager from '../../events/EventManager';

EventManager.add_action('init_content', function(root = document)
{
	let emails					= root.querySelectorAll('input[type="email"]');
	let fillers					= [
		"@gmail.com",
		"@googlemail.com",
		"@yahoo.co.uk",
		"@yahoo.com",
		"@hotmail.co.uk",
		"@hotmail.com",
		"@facebook.com",
		"@mac.com",
		"@aol.com"
	];

	[].forEach.call(emails, function(input, i)
	{
		events.on(input, 'keyup', function(ev)
		{
			let value			= this.value;
			let parts			= value.split('@');
			let matches			= [];

			if(parts.length === 2 && parts[1].length)
			{
				let re			= new RegExp('@' + parts[1], 'i');

				for(var i  = 0; i < fillers.length; i++)
				{
					if(fillers[i].match(re))
					{
						let str	= parts[0] + fillers[i];

						if(str !== value)
						{
							matches.push(str);
						}
					}
				}

				if(matches.length)
				{
					// TODO: implement a visual method for selecting the options
				}
			}
		});
	});

}, 10, 3);
