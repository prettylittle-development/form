
import {dom} from '../../dom/dom';
import {events} from '../../events/events';
import EventManager from '../../events/EventManager';

EventManager.add_action('init_content', function(root = document)
{
	// show/hide
	let show_hide			= document.querySelectorAll('.field__showhide');

	// loop through all the password show/hide items
	[].forEach.call(show_hide, function(toggle, i)
	{
		events.on(toggle, 'click', function(ev)
		{
			let closest			= dom.getClosest(this, '.field__input');
			let input			= closest.querySelector('input');
			let type			= input.getAttribute('type');

			if(type === 'password')
			{
				input.setAttribute('data-validator', 'password');
				input.setAttribute('type', 'text');
			}
			else
			{
				input.removeAttribute('data-validator');
				input.setAttribute('type', 'password');
			}
		});
	});
});
