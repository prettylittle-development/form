
import {dom} from '../../dom/dom';
import {events} from '../../events/events';
import EventManager from '../../events/EventManager';

EventManager.add_action('page_ready', function()
{
	let step_validate			= function(field)
	{
		let step				= dom.getClosest(field, '[data-multistep]');

		if(step)
		{
			let inputs			= step.querySelectorAll('input, textarea, select');
			let valid			= true;

			[].forEach.call(inputs, function(field, i)
			{
				let result		= EventManager.do_action('field/validate', field, true, false);

				if(result)
				{
					valid		= false;

					return;
				}
			});

			if(valid)
			{
				dom.addClass(step, 'valid');
				dom.removeClass(step, 'invalid');
			}
			else
			{
				dom.removeClass(step, 'valid');
				dom.addClass(step, 'invalid');
			}
		}
	}

	EventManager.add_action('form/field/valid', step_validate);
	EventManager.add_action('form/field/invalid', step_validate);
});
