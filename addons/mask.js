
import {dom} from '../../dom/dom';
import {events} from '../../events/events';
import EventManager from '../../events/EventManager';

EventManager.add_action('init_content', function(root = document)
{
	let inputs					= root.querySelectorAll('input[data-mask], textarea[data-mask]');

	// setting up placeholder masking
	// loop through all th einputs on page load, and apply the relevant classes
	[].forEach.call(inputs, function(input, i)
	{
		let span				= document.createElement('span');
			span.className		= "field__mask";

		let value				= input.value;

			span.innerHTML		= "<span>"+value+"</span>" + input.dataset.mask.substr(value.length);

		dom.insertAfter(input, span);

		// TODO: input event doesn't allow you to trap the keycode, but works much better for formatting
		events.on(input, 'input', function(ev)
		{
			let value			= this.value;
			let mask			= this.dataset.mask;

			if(ev.keyCode !== 8)
			{
				let specials		= '/- ()'.split('');

				if(value.length < mask.length)
				{
					let char		= mask.charAt(value.length);

					if(specials.indexOf(char) > -1)
					{
						value	   += char;

						this.value	= value;
					}
				}
			}

			value		= value.substr(0, mask.length);

			input.value	= value;

			span.innerHTML		= "<span>"+value+"</span>" + mask.substr(value.length);
		});
	});

}, 10, 3);
