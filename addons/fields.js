
import {dom} from '../../dom/dom';
import {events} from '../../events/events';
import EventManager from '../../events/EventManager';

EventManager.add_action('init_content', function(root = document)
{
	// function to toggle the has-value class on a input
	let _fieldvalue = function(ev)
	{
		if(this.type === 'hidden')
		{
			return;
		}

		let val					= this.value;

		if(val === "")
		{
			dom.removeClass(this, "has-value");
		}
		else
		{
			dom.addClass(this, "has-value");
		}
	}

	// get all the inputs
	let inputs					= root.querySelectorAll('input, textarea');

	// loop through all the inputs on page load, and apply the relevant classes
	[].forEach.call(inputs, function(field, i)
	{
		_fieldvalue.call(field);
	});

	// when the field focus is left, check to see if the field has a value or not
	events.live('input, textarea', 'focusout', _fieldvalue);

	// when the field is focused, check to see if it has a value or not, this is an additional safety check as fields may be autofilled
	events.live('input, textarea', 'focusin', _fieldvalue);

}, 10, 3);
