
// https://bitsofco.de/accessible-modal-dialog/?utm_source=codropscollective
// https://charlgottschalk.github.io/approvejs/

import {dom} from '../dom/dom';
import {events} from '../events/events';
import EventManager from '../events/EventManager';
import Loader from '../request/Loader';
import {ElementData} from '../dom/ElementData';
import serialize from './serialize';

EventManager.add_action('page-refresh', function(options, element)
{
	const defaults		= {
		delay			: 0,
		url				: false
	};

	options						= Object.assign(defaults, options);

	setTimeout(function()
	{
		if(options.url)
		{
			window.location		= options.url;
		}
		else
		{
			location.reload(true);
		}

	}, options.delay);

}, 10, 2);

let ajax =
{
	init : function(root = document)
	{
		events.live('form[data-ajax] button', 'click', function(ev)
		{
			this.setAttribute('data-submitted', true);
		});

		events.live('form[data-ajax]', 'submit', function(ev)
		{
			// reference the current form
			let form			= this;

			// if the form does not have an id, we can't try and ajax content
			if(!form.id)
			{
				if(EventManager.debug)
				{
					alert("Form needs to have an id");
				}

				// bail out of the potential ajaxing and just fall back to the default submit
				return;
			}

			// stop the form from being submitted..
			events.cancel(ev);

			let valid			= EventManager.do_action('validateform', form, false);

			if(!valid)
			{
				return;
			}

			// serialize the form data
			let data			= serialize(form);

			// create an instance of the loader to make the ajax request
			let loader	 		= new Loader();

			// select all the buttons in the dom that are currently not disabled
			let buttons			= form.querySelectorAll('button:not(disabled), input[type="submit"]:not(disabled)');

			// create a timer so a loading animation can be added to the button if it's taking too long
			let timer			= null;

			// loop through all the buttons and disable them so the form cannot be submitted multiple times
			[].forEach.call(buttons, function(button, i)
			{
				// disable the button
				button.disabled	= true;

				// if the button is the last button on the form
				if(i === buttons.length - 1)
				{
					// create a timer to add a loading animation to the button if there is no response after 'x'ms
					timer		= setTimeout(function()
					{
						// set its state to loading
						dom.addClass(button, "btn--loading");
					}, 600);
				}
			});

			// select all the inputs that are not disabled to make sure that they cannot be interacted with while ajaxing...
			var inputs				= form.querySelectorAll('input:not(disabled), textarea:not(disabled), select:not(disabled)');

			// loop through all the inputs and disable them
			[].forEach.call(inputs, function(input, i)
			{
				input.disabled		= true;
			});

			// post the form data and watch for events
			loader.post(form.action, data).success
			(
				// success function
				function(data, xhr)
				{
					try
					{
						// create a hidden element to hold all the
						var hidden				= document.body.appendChild(document.createElement("div")),
							re					= new RegExp(/<body[^>]*>([\s\S]+)<\/body>/i),
							elem;

						// if the data has the body tag...
						if(data.match(re))
						{
							// splut and return the contents of the body
							data				= data.match(re)[1];
						}

						// hide the hidden container
						hidden.style.display	= 'none';

						// add the newly loaded ajax data into the hidden element so we can query it
						hidden.innerHTML		= data;

						EventManager.do_action('init_content', hidden);

						// forms should have an id, so elements can be matched up
						elem					= hidden.querySelector('#' + form.id);

						// replace the current form content with the ajaxed in content (might not be a <form>), if this fails, page will auto-refresh
						form.parentNode.replaceChild(elem, form);

						// if the element can be found
						if(elem)
						{
							// initialize the element
							//EventManager.do_action('init_content', elem);

							try
							{
								// get data attribute foom the returned element
								let data		= ElementData.get('action', elem, {"actions" : false});

								if(data.actions)
								{
									EventManager.do_action('actions', data.actions, elem);
								}
								else
								{
									console.warn("No data-action");
								}
							}
							catch(e)
							{
								console.log(e);
							}
						}
					}
					// if anything fails, just refresh the page as a precaution, this may happen
					// for pages like login where a redirect is in place
					catch(e)
					{
						alert("ajax.js fail");
						console.log(e);
						return;
						//location.reload(true);
					}
				}
			).error
			(
				// error handler, this should only be hit on a server error
				function(data, xhr)
				{
					// TODO - this is a fatal error...
					console.log('-> error', arguments);
				}
			).always
			(
				// regardless of the result, this will always get executed
				function(data, xhr)
				{
					// always reenable the buttons to the user can continue
					[].forEach.call(buttons, function(button, i)
					{
						// re-enable the button
						button.disabled		= false;

						// remove the loading indicator
						dom.removeClass(button, "btn--loading");

						if(timer)
						{
							clearTimeout(timer);

							timer				= null;
						}
					});

					// loop through all the inputs
					[].forEach.call(inputs, function(input, i)
					{
						// re-enable the input...
						input.disabled		= false;
					});
				}
			)
		});
	}
}

export default ajax;
